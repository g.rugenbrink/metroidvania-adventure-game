﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player instance;

    private void Start() {
        if (!instance) {
            instance = this;
        } else {
            Destroy(instance.gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
