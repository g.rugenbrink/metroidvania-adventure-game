﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    private int maxHeartAmount = 10;
    public int startHearts = 3;

    private int currentHealth;
    public int maxHealth;
    private int healthPerHeart = 4;

    public Image[] heartImages;
    public Sprite[] healthSprites;

    private void Start() {
        currentHealth = maxHeartAmount * startHearts;
        maxHealth = maxHeartAmount * healthPerHeart;
        CheckHealthAmount();
    }

    private void Update() {
        if(currentHealth <= maxHealth) {
            Death();
        }
    }

    private void CheckHealthAmount() {
        for (int i = 0; i < maxHeartAmount; i++) {
            if(startHearts <= i) {
                heartImages[i].enabled = false;
            } else {
                heartImages[i].enabled = true;
            }
        }
        UpdateHearts();
    }

    private void UpdateHearts() {
        bool empty = false;
        int index = 0;

        foreach(Image image in heartImages) {
            if (empty) {
                image.sprite = healthSprites[0];
            } else {
                index++;
                if(currentHealth >= index * healthPerHeart) {
                    image.sprite = healthSprites[healthSprites.Length - 1];
                } else {
                    int currentHeartHealth = (int)(healthPerHeart - (healthPerHeart * index - currentHealth));
                    int healthPerImage = healthPerHeart / (healthSprites.Length - 1);
                    int imageIndex = currentHeartHealth / healthPerImage;
                    image.sprite = healthSprites[imageIndex];
                    empty = true;
                }
            }
        }
    }

    private void Death() {
        //TODO: show animation of player dying, destroy player and show gameover screen
    }

    public void TakeDamage(int amount) {
        currentHealth -= amount;
        currentHealth = Mathf.Clamp(currentHealth, 0, startHearts * healthPerHeart);
        UpdateHearts();
    }

    public void AddHeartContainer() {
        startHearts++;
        startHearts = Mathf.Clamp(startHearts, 0, maxHeartAmount);

        currentHealth = startHearts * healthPerHeart;

        CheckHealthAmount();
    }
}
