﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour
{
    private new Rigidbody2D rigidbody;
    private Vector2 aimVector;
    public float aimAngle;
    private PlayerMovement playerMovement;

    private void Start() {
        playerMovement = GetComponent<PlayerMovement>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if(rigidbody.velocity != Vector2.zero) {
            UpdateAim(playerMovement.lastDirection);
        }
    }

    private void UpdateAim(Vector2 dir) {
        //Get the direction vector normalized for multiplicaation
        aimVector = dir;
        aimVector.Normalize();

        //Adjust the aim angle
        aimAngle = MathUtil.AngleFromVector(aimVector);
    }
}
