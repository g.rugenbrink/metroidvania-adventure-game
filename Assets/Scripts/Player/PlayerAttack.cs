﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField]
    private Collider2D[] weaponColliders = null;
    
    public bool isAttacking = false;

    [SerializeField]
    private float minTimeBetweenattacks = .25f;
    private float currentTimeLeft;
    private int currentAttack = 0;

    private Animator animator;

    private void Start() {
        animator = GetComponent<Animator>();
    }

    private void Update() {
        currentAttack = (int)Mathf.Repeat(currentAttack, weaponColliders.Length);
        if (Time.time >= currentTimeLeft) {
            isAttacking = false;
            for (int i = 0; i < weaponColliders.Length; i++) {
                weaponColliders[i].enabled = false;
            }
            if (Input.GetButtonDown("Fire1") && !isAttacking) {
                Attack();
            }
        }
        animator.SetBool("IsAttacking", isAttacking);
    }

    private void Attack() {
        isAttacking = true;
        weaponColliders[currentAttack].enabled = true;
        currentTimeLeft = Time.time + minTimeBetweenattacks;
        currentAttack++;
    }
}
