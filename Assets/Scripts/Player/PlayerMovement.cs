﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    private new Rigidbody2D rigidbody;
    private Vector2 previousLocation;
    [HideInInspector]
    public Vector2 lastDirection;
    private Animator animator;

    private void Start() {
        previousLocation = Vector2.zero;
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Update() {
        Movement();
    }

    private void Movement() {
        //Sets the lastdirection and normalizes it
        //so it returns a value between 0 and 1 for the x and y axis
        if ((Vector2)transform.position - previousLocation != Vector2.zero &&
            new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) != Vector2.zero) {
            lastDirection = (Vector2)transform.position - previousLocation;
            lastDirection.Normalize();
            animator.SetBool("IsMoving", true);
        } else if(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) == Vector2.zero) {
            animator.SetBool("IsMoving", false);
        }
        //Gets the input of the player
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        //Store the input in a vector, normalize it for multiplication
        //and add it to the velocity of the rigidbody
        Vector2 input = new Vector2(horizontal, vertical);
        rigidbody.AddForce(input * moveSpeed * Time.deltaTime);
        animator.SetFloat("velocityX", lastDirection.x);
        animator.SetFloat("velocityY", lastDirection.y);
        previousLocation = transform.position;
    }
}
