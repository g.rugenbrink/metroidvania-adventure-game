﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathUtil : MonoBehaviour
{
    public static Vector3 VectorFromAngle(float dir) {
        return new Vector3(Mathf.Cos(Mathf.Deg2Rad * dir), Mathf.Sin(Mathf.Deg2Rad * dir));
    }

    public static float AngleFromVector(Vector2 dir) {
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }
}
