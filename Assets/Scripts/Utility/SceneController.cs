﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static SceneController instance;
    public static int currentSceneIndex;
    public Animator animator;

    private void Awake() {
        DontDestroyOnLoad(gameObject);
        if (!instance) {
            instance = this;
        } else {
            Destroy(this.gameObject);
        }
    }

    public void FadeToLevel(int sceneIndex) {
        currentSceneIndex = sceneIndex;
        animator.SetTrigger("FadeOut");
    }

    public void LoadScene() {
        SceneManager.LoadScene(currentSceneIndex);
        animator.SetTrigger("FadeIn");
    }

    public static string GetCurrentSceneName() {
        return SceneManager.GetSceneAt(currentSceneIndex).name;
    }
}
