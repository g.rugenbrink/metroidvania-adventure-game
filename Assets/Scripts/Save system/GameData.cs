﻿using UnityEngine;

[System.Serializable]
public class GameData
{
    public static GameData current;
    public int lastScene;
    public GameObject player;

    public GameData(int lastScene, GameObject player) {
        this.lastScene = lastScene;
        this.player = player;
    }
}
