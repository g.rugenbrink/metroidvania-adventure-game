﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using System;

public static class SaveSystem
{

    public static void Save() {
        try {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/player.data";
            FileStream stream = new FileStream(path, FileMode.Create);

            //TODO: Add current Sceneindex in here
            GameData data = new GameData(SceneController.currentSceneIndex, Player.instance.gameObject);

            formatter.Serialize(stream, data);
            stream.Close();
        } catch (Exception ex) {
            Debug.LogError(ex.Message);
        }
    }

    public static GameData Load() {
        string path = Application.persistentDataPath + "/player.data";
        try {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GameData data = (GameData)formatter.Deserialize(stream);
            stream.Close();

            return data;
        } catch (Exception ex) {
            Debug.Log(ex.Message);
            return null;
        }
    }
}