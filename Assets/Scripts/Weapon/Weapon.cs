﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public int damage = 1;
    [SerializeField]
    private float knockbackForce = 750.0f;

    private void OnTriggerEnter2D(Collider2D collision) {
        //TODO: Add damage enemy logic
        if (collision.GetComponent<EnemyHealth>()) {
            Vector2 direction = (collision.GetComponentInParent<Transform>().position - transform.position);
            direction.Normalize();
            direction *= knockbackForce;
            collision.GetComponent<EnemyHealth>().TakeDamage(damage, direction);
        }
    }
}
