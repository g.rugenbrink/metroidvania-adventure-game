﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    public EnemyAim enemyAim;
    public Transform upPosition;
    public Transform downPosition;
    public Transform leftPosition;
    public Transform rightPosition;

    public int damage = 1;

    private void Update() {
        if (enemyAim.aimAngle >= -45 && enemyAim.aimAngle < 45) {
            transform.position = rightPosition.position;
            transform.eulerAngles = rightPosition.eulerAngles;
        } else if (enemyAim.aimAngle >= 45 && enemyAim.aimAngle < 135) {
            transform.position = upPosition.position;
            transform.eulerAngles = upPosition.eulerAngles;
        } else if (enemyAim.aimAngle >= 135 || enemyAim.aimAngle < -135) {
            transform.position = leftPosition.position;
            transform.eulerAngles = leftPosition.eulerAngles;
        } else if (enemyAim.aimAngle >= -135 && enemyAim.aimAngle < -45) {
            transform.position = downPosition.position;
            transform.eulerAngles = downPosition.eulerAngles;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<PlayerHealth>()) {
            collision.GetComponent<PlayerHealth>().TakeDamage(damage);
        }
    }
}
