﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    private int currentHealth;
    public int maxHealth = 2;

    private Rigidbody2D rigidbody;
    private SpriteRenderer spriteRenderer;

    private void Start() {
        currentHealth = maxHealth;
        rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update() {
        if(currentHealth <= 0) {
            Death();
        }
    }

    private void Death() {
        Destroy(gameObject);
    }

    public void TakeDamage(int amount, Vector2 knockbackDirection) {
        currentHealth -= amount;
        rigidbody.AddForce(knockbackDirection);
    }

}
