﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAim : MonoBehaviour
{
    private EnemyMovement enemyMovement;
    private Rigidbody2D rigidbody;
    private Vector2 aimVector;
    public float aimAngle;

    private void Start() {
        enemyMovement = GetComponent<EnemyMovement>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if(rigidbody.velocity != Vector2.zero) {
            UpdateAim(Player.instance.transform.position - transform.position);
        }
    }

    private void UpdateAim(Vector2 direction) {
        aimVector = direction;
        aimVector.Normalize();

        aimAngle = MathUtil.AngleFromVector(aimVector);
    }
}
