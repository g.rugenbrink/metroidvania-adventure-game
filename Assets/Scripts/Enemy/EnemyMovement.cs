﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 1600f;
    [SerializeField]
    private float stopChasingDistance = 1f;
    [SerializeField]
    private float distanceToChase = 10f;
    [SerializeField]
    private float walkRadius = 5f;
    private Transform player;
    private Rigidbody2D rigidbody;
    Vector2 tempPosition;
    [HideInInspector]
    public Vector2 lastDirection;
    private Vector2 previousLocation;


    private void Start() {
        previousLocation = Vector2.zero;
        rigidbody = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<Player>().transform;
        CalculatePositionToWalkTo();
    }

    private void Update() {
        if ((Vector2)transform.position - previousLocation != Vector2.zero) {
            lastDirection = (Vector2)transform.position - previousLocation;
            lastDirection.Normalize();
        }

        if (Vector2.Distance(player.position, transform.position) < distanceToChase) {
            ChasePlayer();
        } else {
            WalkAround();
        }
        previousLocation = transform.position;
    }

    private void ChasePlayer() {
        Vector2 direction = player.position - transform.position;
        direction.Normalize();
        if ((player.position - transform.position).magnitude > stopChasingDistance) {
            rigidbody.AddForce(direction * movementSpeed * Time.deltaTime);
        }
    }

    private void WalkAround() {
        Vector2 direction = (tempPosition - (Vector2)transform.position);
        direction.Normalize();
        rigidbody.AddForce(direction * movementSpeed * Time.deltaTime);
        if((tempPosition - (Vector2)transform.position).magnitude < 0.1f) {
            CalculatePositionToWalkTo();
        }
    }

    private void CalculatePositionToWalkTo() {
        tempPosition = (Vector2)transform.position + (Random.insideUnitCircle * walkRadius);
    }
}
