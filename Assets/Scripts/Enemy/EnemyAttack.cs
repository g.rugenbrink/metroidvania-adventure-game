﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private Rigidbody2D rigidbody = null;

    [SerializeField]
    private Collider2D weaponCollider = null;

    private bool isAttacking = false;
    [SerializeField]
    private float timeBetweenAttacks = 1f;
    private float attackDuration = 0.1f;
    private float attackTimeLeft;
    private float timeLeft;
    [SerializeField]
    private float attackRange = 1.25f;
    [SerializeField]
    private float velocityToAttack = 0.15f;

    private void Start() {
        isAttacking = false;
        weaponCollider.enabled = false;
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if(rigidbody.velocity.magnitude <= velocityToAttack && Vector2.Distance(transform.position, Player.instance.transform.position) < attackRange) {
            if (Time.time >= timeLeft && !isAttacking) {
                Attack();
            } else {
                isAttacking = false;
                if (Time.time >= attackTimeLeft) {
                    weaponCollider.enabled = false;
                }
            }
        }
    }

    private void Attack() {
        isAttacking = true;
        weaponCollider.enabled = true;
        timeLeft = Time.time + timeBetweenAttacks;
        attackTimeLeft = Time.time + attackDuration;
    }

    private void OnDrawGizmos() {
        if (isAttacking) {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(weaponCollider.transform.position, 0.5f);
        }
    }
}
