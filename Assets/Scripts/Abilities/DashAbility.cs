﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAbility : Ability {
    [SerializeField]
    private float dashSpeed = 70000f;
    private PlayerMovement playerMovement;
    private Rigidbody2D rigidbody;

    private void Awake() {
        playerMovement = GetComponent<PlayerMovement>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if (Input.GetButtonDown("Dash")) {
            ExecuteAbility();
        }
    }

    public override void ExecuteAbility() {
        rigidbody.AddForce(playerMovement.lastDirection * dashSpeed * Time.deltaTime, ForceMode2D.Force);
    }
}
