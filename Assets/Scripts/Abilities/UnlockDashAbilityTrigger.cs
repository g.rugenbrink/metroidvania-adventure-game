﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockDashAbilityTrigger : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<Player>()) {
            collision.gameObject.AddComponent<DashAbility>();
            Destroy(this.gameObject);
        }
    }
}
